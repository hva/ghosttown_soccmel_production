var conf = require((process.argv.length==3)?process.argv[2]:'./conf.js');
var app = require('koa')();
var body = require('koa-body')();
var router = require('koa-router')({prefix: conf.prefix});
var render = require('co-ejs');
var send = require('koa-send');
var db = require('monk')(conf.mongo);
var usr = require('co-monk')(db.get('usr'));
var theme = require('co-monk')(db.get('theme'));
var fs = require('fs');

var idleRegMins = 60;
var idleSessMins = 15;

var maxage = 3600*24*1000;


app.proxy = true;

app.use(require('koa-conditional-get')());

app.use(require('koa-etag')());

app.use(function * (next) {
    var start = new Date;

    yield next;

    var ms = new Date - start;

    console.log('%s %s %s %s %s %sms "%s"\n\n',
		this.request.ip,
		(new Date).toJSON(),
		this.method,
		this.url,
		this.status,
		ms,
		this.request.headers['user-agent']);
});


app.use(router.routes());


app.use(router.allowedMethods());


app.use(render(app, {layout: false, root:__dirname + '/ejs'}));


router.get('media/:file', function * () {
    yield send(this, 'media/' + this.params.file, {maxage: maxage});
});


router.get('css/:file', function * () {
    yield send(this, 'css/' + this.params.file, {maxage: maxage});
});


router.get('js/:file', function * () {
    yield send(this, 'js/' + this.params.file, {maxage: maxage});
});


router.post('newu', body, function * () {
    var u = this.request.body.user;
    var e = this.request.body.email;
    var p = this.request.body.password;
    var t = (new Date).getTime();
    var h = sha3(u + e + t);
    var status = {};
    var r, id;

    yield cleanRegistration();
    if(yield usr.findOne({usr: u})) {
	this.body = '{"err": 1, "message": "username not available"}';
	return;
    }
    yield regEmail(this.ip, u, e, h);
    yield usr.insert({usr: u, pass: p, email: e, tstamp: t,  sess: [], mat: [], reg: h,
		      status: 0, blogs: 0, comments: 1, contributes: 1, isBofh: 0});
    this.body = '{"message": "We have tried to send a verification email to ' + e +
	',<br />please check your mailbox and confirm the registration"}';
});


router.post('auth', body, function * () {
    var u = this.request.body.user;
    var p = this.request.body.password;

    r = yield usr.findOne({usr: u, status: 1});
    if(!r) {
	this.body = '{"err": 1, "message": "username not registered"}';
	return;
    }
    if(r.pass!=p) {
	this.body = '{"err": 2, "message": "wrong password"}';
	return;
    }
    yield newSess(this, u);
    this.body = '{"message": "' + conf.prefix + 'space_of_' + u + '"}';
});


router.get('confirm/:hash', function * () {
    var r;

    yield cleanRegistration();
    r = yield usr.findOne({reg: this.params.hash});
    if(!r) {
	this.response.redirect(conf.prefix + 'home');
	return;
    }

    yield usr.update({reg: this.params.hash}, {$unset: {reg: ''}, $set: {status: 1}});
    yield newSess(this, r.usr);
    this.response.redirect(conf.prefix + 'space_of_' + r.usr);
});


router.get('themes', function * () {
    var resp = [];

    (yield theme.find({})).forEach(function (e) {if(e.mat.length) resp.push(e.theme);});
    this.body = '{"message": ' + JSON.stringify(resp) + '}';
});


router.post('themes/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    var url = this.request.body.url;
    var i, rec = yield usr.findOne({usr: u}, {_id: 0, mat: 1});

    for (i = 0; i<rec.mat.length && rec.mat[i].url != url; i++);
    this.body = '{"message": ' + JSON.stringify(rec.mat[i].theme) + '}';
});


router.post('unpublish/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    var url = this.request.body.url;

    yield usr.update({usr: u, mat: {$elemMatch: {url: url}}}, {$set: {'mat.$.theme': []}});
    yield theme.update({}, {$pull: {mat: {usr:u, url: url}}}, {multi: true});
    yield theme.remove({mat: []});
    yield notifyUnpublish(u, url, this.request.body.descr);
    this.body = '{"message": "space_of_' + u + '"}';
});


router.post('themeAdd/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    var url = this.request.body.url;
    var t = this.request.body.theme;
    var i, rec;

    if(!(yield theme.findOne({theme: t}))) yield theme.insert({theme: t});
    yield theme.update({theme: t}, {$addToSet: {mat: {usr: u, url: url}}});
    if(yield usr.findOne({usr: u, mat: {$elemMatch: {url: url, theme:[]}}})) yield notifyPublish(u, url, this.request.body.descr);
    yield usr.update({usr: u, mat: {$elemMatch: {url: url}}}, {$addToSet: {'mat.$.theme': t}});
    rec = yield usr.findOne({usr: u}, {_id: 0, mat: 1});
    for (i = 0; i<rec.mat.length && rec.mat[i].url != url; i++);
    this.body = '{"message": ' + JSON.stringify(rec.mat[i].theme) + '}';
});


router.post('themeDel/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    var url = this.request.body.url;
    var t = this.request.body.theme;
    var i, rec, resp;

    yield theme.update({theme: t}, {$pull: {mat: {usr: u, url: url}}}, {multi: true});
    yield theme.remove({mat: []});
    rec = yield usr.findOne({usr: u}, {_id: 0, mat: 1});
    for (i = 0; i<rec.mat.length && rec.mat[i].url != url; i++);
    resp = rec.mat[i].theme.filter(function (e) {return (e != t);});
    if(!resp.length) yield notifyUnpublish(u, url, this.request.body.descr);
    yield usr.update({usr:u, mat: {$elemMatch: {url: url}}}, {$set: {'mat.$.theme': resp}});
    this.body = '{"message": ' + JSON.stringify(resp) + '}';
});


router.post('contribs/:user', body, function * () {
    if(!(yield checkSess(this, this.params.user, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    var resp = yield usr.findOne({usr: this.request.body.usr}, {sess: 0, status: 0});
    var contribs = [];

    resp.mat.forEach(function (e) {if(e.theme.length) contribs.push({url: e.url, descr: e.descr});});
    resp.mat = contribs;
    this.body = '{"message": ' + JSON.stringify(resp) + '}';
});


router.post('notification/:user', body, function * () {
    var u = this.params.user;
    var isNotified = this.request.body.isNotified;

    if(!(yield checkSess(this, u, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    yield usr.update({usr: this.params.user}, {$set: {isNotified: isNotified}});
    isNotified = (yield usr.findOne({usr: u, isNotified: 1}))?1:0;
    this.body = '{"message": {"isNotified": ' +  isNotified +'}}';
});


router.post('role/:user', body, function * () {
    if(!(yield checkSess(this, this.params.user, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    var role = {};

    role[this.request.body.role] = this.request.body.enabled;
    yield usr.update({usr: this.request.body.usr}, {$set: role});
    this.body = '{"message": "role set"}';
});


router.post('forcePassword/:user', body, function * () {
    if(!(yield checkSess(this, this.params.user, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    yield usr.update({usr: this.request.body.usr}, {$set: {pass: this.request.body.pass}});
    this.body = '{"message": "password set by force"}';
});


router.post('forceEmail/:user', body, function * () {
    if(!(yield checkSess(this, this.params.user, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    yield usr.update({usr: this.request.body.usr}, {$set: {email: this.request.body.email}});
    this.body = '{"message": "password set by force"}';
});


router.post('forceUnpublish/:user', body, function * () {
    if(!(yield checkSess(this, this.params.user, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    var u = this.request.body.usr;
    var url = this.request.body.url;

    yield usr.update({usr: u, mat: {$elemMatch: {url: url}}}, {$set: {'mat.$.theme': []}});
    yield theme.update({}, {$pull: {mat: {usr:u, url: url}}}, {multi: true});
    yield theme.remove({mat: []});
    yield notifyForceUnpublish(this.params.user, this.request.body.reason, u, url, this.request.body.descr);
    this.body = '{"message": "unpublished by force"}';
});


router.get('contribs', function * () {
    var resp = {theme: {}, mat: {}};

    (yield theme.find({})).forEach(function (e) {
	if(e.mat.length) resp.theme[e.theme] = e.mat;
    });
    (yield usr.find({mat: {$elemMatch: {theme: {$ne: []}}}})).forEach(function (e) {
	if(e.mat.length) {
	    resp.mat[e.usr] = {};
	    e.mat.forEach(function (d) {
		if(d.theme.length)
		    resp.mat[e.usr][d.url] = {tstamp: d.tstamp, descr: d.descr, theme: d.theme};
	    });
	}
    });
    this.body = '{"message": ' + JSON.stringify(resp) + '}';
});


router.get('show' , function *() {
    yield this.render('show', {title: conf.self+' show ', ip: this.ip});
});

router.get('home', function * () {
    yield this.render('home', {title: conf.self + ' home ', ip: this.ip});
});


router.post('home',  body, function * () {
    var u = this.request.body.usr;

    if(yield checkSess(this, u, 'contributes'))
	yield this.render('public', {title: conf.self + ' home, ' + u, ip: this.ip, usr: u});
    else this.response.redirect(conf.prefix + 'home');
});


router.get('space_of_:user', function * () {
    var u = this.params.user;

    if(yield checkSess(this, u, 'contributes'))
	yield this.render('user', {title: conf.self + ' for ' + u, usr: u, ip: this.ip});
    else
	this.response.redirect(conf.prefix + 'home');
});


router.get('admin_by_:user', function * () {
    var u = this.params.user;

    if(yield checkSess(this, u, 'isBofh'))
	yield this.render('admin', {title: conf.self + ', administrator: ' + u, usr: u, ip: this.ip});
    else
	this.response.redirect(conf.prefix + 'home');
});


router.get('usersAndNotification/:user', function * () {
    var u = this.params.user;
    var users = [], notified;

    if(!(yield checkSess(this, u, 'isBofh'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    notified = (yield usr.findOne({usr: u, isNotified: 1}))?1:0;
    (yield usr.find({}, {_id: 0, usr: 1})).forEach(function (e) {if(e.usr != u) users.push(e.usr);});
    this.body = '{"message": ' + JSON.stringify({usr: users, isNotified: notified}) + '}';
});


router.post('matUpload/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    var dir = 'archive/' + u;

    if(!checkDir(dir))
	return;

    var parts = require('co-busboy')(this, {autoFields: true});
    var part;

    while ((part = yield parts)) {
	var url = dir + '/' + part.filename;

	if(url.slice(-4).toLowerCase()=='.pdf') url = url.slice(0, -3) + 'pdf';
	if(yield usr.findOne({usr:u, mat: {$elemMatch: {url: url}}})) {
	    this.body = '{"err": 2, "message": "' + part.filename + ' already uploaded"}';
	    return;
	}

        var stream = fs.createWriteStream(url);
	var d = parts.field['descr'];
	var descr = (d)?((d.length)?d:part.filename):part.filename;

	part.pipe(stream);
	console.log('\tupload: ', part.filename + ' copied to ' +  stream.path);
	yield usr.update({usr: u}, {$push: {mat: {url: url, theme: [], tstamp: (new Date).getTime(), descr: descr}}});
	if(url.slice(-4) == '.pdf')
	    yield pdfCover(url);
    }
    this.body = '{"message": "space_of_' + u + '"}';
});


router.post('urlUpload/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    var url = this.request.body.url;
    var d = this.request.body.descr;
    var descr = (d)?((d.length)?d:url):url;

    if(yield usr.findOne({usr:u, mat: {$elemMatch: {url: url}}})) {
	this.body = '{"err": 2, "message": "' + url + ' already uploaded"}';
	return;
    }
    yield usr.update({usr: u}, {$push: {mat: {url: url, theme: [], tstamp: (new Date).getTime(), descr: unesc(descr)}}});
    this.body = '{"message": "space_of_' + u + '"}';
});


router.post('matDelete/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    var f = this.request.body.file;

    if(f.indexOf('http'))
	fs.unlink(f, function(err) {console.log('\tdelete: ' + f + ' deleted');});
    yield theme.update({}, {$pull: {mat: {usr:u, url: f}}}, {multi: true});
    yield theme.remove({mat: []});
    if(yield usr.findOne({usr: u, mat: {$elemMatch: {url: f, theme: {$ne: []}}}}))
	yield notifyUnpublish(u, f, this.request.body.descr);
    yield usr.update({usr: u}, {$pull: {mat: {url: f}}}, {multi: true});
    this.body = '{"message": "space_of_' + u + '"}';
});


router.post('matDescr/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u, 'contributes'))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }

    yield usr.update({usr: u, mat: {$elemMatch: {url: this.request.body.url}}},
		     {$set: {'mat.$.descr': unesc(this.request.body.descr)}});
    this.body = '{"message": "space_of_' + u + '"}';
});


router.post('password/:user', body, function * () {
    var u = this.params.user;

    if(!(yield checkSess(this, u))) {
	this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
	return;
    }
    if(!(yield usr.findOne({usr:u, pass: this.request.body.old}))) {
       	this.body = '{"err": 2, "message": "wrong (old) password"}';
	return;
    }

    yield usr.update({usr: u}, {$set: {pass: this.request.body.new}});
    this.body = '{"message": "space_of_' + u + '"}';
});


router.get('archive/:user/:file', function * () {
    var u = this.params.user;
    var url = 'archive/' + u + '/' + this.params.file;

    if(yield usr.findOne({usr: u, mat: {$elemMatch: {url: url, theme: {$ne: []}}}}))
	yield send(this, url);
    else if(yield checkSess(this, u, 'contributes'))
	yield send(this, url);
    else if(url.slice(-4) == '.png') {
	if(yield usr.findOne({usr: u, mat: {$elemMatch: {url: url.slice(0, -3) + 'pdf', theme: {$ne: []}}}}))
	    yield send(this, url);
	else this.response.redirect(conf.prefix + 'home');
    }

    else this.response.redirect(conf.prefix + 'home');
});


router.get('matList/:user', function * () {
    var u = this.params.user;

    if(yield checkSess(this, u, 'contributes'))
	this.body =  '{"message": ' + JSON.stringify(yield matlist(u)) + '}';
    else this.body = '{"err": 1, "message": "' + conf.prefix + 'home"}';
});


router.get('logout/:user', function * () {
    yield killSess(this);
    this.response.redirect(conf.prefix + 'home');
});


app.use(function * (next) {
    this.response.redirect(conf.prefix + 'home');
});


app.listen(conf.listen.port, conf.listen.host);



var esc = function (s) {
    return encodeURIComponent(s).replace(/[!'()*]/g, function(c) {
	return '%' + c.charCodeAt(0).toString(16);
    });
};


var unesc = decodeURIComponent;


var checkDir = function(dir) {
    try {
	fs.statSync(dir);
    } catch (err) {
	if(err.code=='ENOENT') {
	    try {
		fs.mkdirSync(dir);
		console.log('\tfs: '  + dir + ' created');
	    } catch (err) {
		console.log('\tfs mkdir: '  + err.message);
		return false;
	    }
	} else {
	    console.log('\tfs: '  + err.message);
	    return false;
	}
    }

    return true;
};


var sha3 = function (x) {
    var s = require('sha3');
    var d = new s.SHA3Hash(256);

    d.update(x);
    return d.digest('hex');
};


var cleanSess = function * () {
    yield usr.update({}, {$pull: {sess: {tstamp: {$lt: (new Date).getTime() - idleSessMins*60*1000}}}}, {multi: true});
};


var checkSess = function * (ctx, u, role) {
    var h = ctx.cookies.get(u);

    if(!h) return false;

    var t = (new Date).getTime();
    var q = {usr: u, sess: {$elemMatch: {hash: h, ip: ctx.ip, tstamp: {$gt: t - idleSessMins*60*1000}}}};

    if(role) q[role] = 1;
    yield cleanSess();
    if(!(yield usr.findOne(q)))
	return false;
    yield usr.update({usr: u, sess: {$elemMatch: {hash: h}}}, {$set: {'sess.$.tstamp': t}});

    return true;
};


var killSess = function * (ctx) {
    var u = ctx.params.user;
    var h = ctx.cookies.get(u);

    if(!h) return;
    ctx.cookies.set(u, '');
    yield usr.update({usr: u}, {$pull: {sess: {hash: h}}}, {multi: true});
    yield cleanSess();
};


var newSess = function * (ctx, u) {
    var t = (new Date).getTime();
    var h = sha3(u + ctx.ip + t);

    ctx.cookies.set(u, h);
    yield usr.update({usr: u}, {$push: {sess: {tstamp: t, hash: h, ip: ctx.ip}}});
    yield cleanSess();
};


var cleanRegistration = function * () {
    yield usr.remove({status: 0, tstamp: {$lt: (new Date).getTime() - idleRegMins*60*1000}});
};


var matlist = function * (u) {
    var r;

    r = yield usr.findOne({usr: u}, {mat: 1});
    if (!r) return '[]';
    return r.mat.sort(function (a, b) {return b.tstamp - a.tstamp;});
};


var pdfCover = function * (url) {
    yield (require('co-exec'))('convert "' + url + '[0]" "' + url.slice(0, -3) + 'png"');
};


var mail = function * (obj) {
    var email = require('emailjs').server.connect(conf.smtp);

    email.send(obj, function(err) {
	if(err) {
	    console.log('\tmail to ' + obj.to + ': ' + err.message);
	}
    });
};


var regEmail = function * (ip, u, to, hash) {
    var text = 'Dear contributor,\nyou requested (from IP ' + ip +
	') to sign up for sharing your views with ' + conf.host + ', possibly as ' +	u +
	'.\nPlease click on the link below to confirm. Thank you.\n\nhttp://' + conf.host +
	conf.prefix + 'confirm/' + hash;

    yield mail({from: '<' + conf.mailsender + '>', to: to, subject: 'Registration on ' + conf.host, text: text});
};


var urlize  = function (url) {
    return url.indexOf('http') ? 'http://' + conf.host + conf.prefix + url : url;
};


var notifyForceUnpublish = function * (adm, reason, u, url, descr) {
    var to = (yield usr.findOne({usr: u})).email;
    var text = 'Dear ' + u + ', your contribution ' + urlize(url) +
	' (' + unesc(descr) + ') has been suspended by administrator ' + adm + ' for this reason: ' + reason + '.';

    yield mail({from: '<' + conf.mailsender + '>', to: to, subject: 'Contribution suspended.', text: text});
};


var notifyPublish = function * (u, url, descr) {
    var i, adm;
    var subject = 'New publication by ' + u + '.';
    var text = 'Contributor ' + u + ' has published ' + urlize(url) +
	' (' + unesc(descr) + ').';

    adm = yield usr.find({usr: {$ne: u}, isBofh: 1, isNotified: 1}, {email: 1});
    for(i = 0; i<adm.length; i++)
	yield mail({from: '<' + conf.mailsender + '>', to: adm[i].email, subject: subject, text: text});
};


var notifyUnpublish = function * (u, url, descr) {
    var i, adm;
    var subject = 'Publication suspended by author ' + u + '.';
    var text = 'Contributor ' + u + ' has unpublished ' + urlize(url) +
	' (' + unesc(descr) + ').';


    adm = yield usr.find({usr: {$ne: u}, isBofh: 1, isNotified: 1}, {email: 1});
    for(i = 0; i<adm.length; i++)
	yield mail({from: '<' + conf.mailsender + '>', to: adm[i].email, subject: subject, text: text});
};
