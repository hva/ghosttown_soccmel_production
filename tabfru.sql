drop table if exists usr;
create table usr (
	id int not null auto_increment,
	name char(64),
	pass char(64),
	email char(128),
	tstamp bigint,
	status tinyint(1) default 0,
	blogs bool default 0,
	comments bool default 1,
	contributes bool default 1,
	isbofh bool default 0,
	primary key (id)
);

drop table if exists registration;
create table registration (
       usr int,
       hash char(64)
);    

drop table if exists sess;
create table sess (
       usr int,
       tstamp bigint,
       hash char(64),
       ip char(15)
);

drop table if exists themecontrib;
create table themecontrib (
       theme int,
       contrib int
);

drop table if exists matcontrib;
create table matcontrib (
       mat int,
       contrib int
);

drop table if exists theme;
create table theme (
	id int not null auto_increment,
	tag char(128),
	descr varchar(512),
	primary key (id)
);

drop table if exists mat;
create table mat (
	id int not null auto_increment,
	usr int,
	tstamp bigint,
	url char(128),
	descr varchar(512),
	primary key (id)
);

drop table if exists contrib;
create table contrib (
	id int not null auto_increment,
	usr int,
	tstamp bigint,
	descr varchar(2048),
	primary key (id)
);
