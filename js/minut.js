var _ = function () {
    var a_ = {};

    a_.kid = {};
    a_.kid.ret = false;

    a_.id = function(id) {
	return document.getElementById(id);
    };

    var kchk = function(e) {
	var k = (window.event) ? event.keyCode : e.keyCode;

	a_.kid.ret = (k == 13);
	a_.kid.key = k;
    };

    document.onkeydown = kchk;


    a_.toFocus = function(id) {
	a_.id(id).style.display = 'block';
	window.location = '#'+id;
    };


    a_.actvx = function(ids) {
	    var id = a_.id(ids);

	    id.style.display = 'block';
	    id.innerHTML;
    };


    a_.cleanx = function(ids) {
	var id = a_.id(ids);

	id.style.display = 'none';
	id.innerHTML = '&nbsp;';
    };


    a_.hidex = function(ids) {
	var id = a_.id(ids);

	id.style.display = 'none';
	id.innerHTML;
    };


    a_.rndchar = function() {
	var i = Math.floor(50.0 * Math.random());

	return (((i<25)?65:72) + i);
    };


    a_.rndnam = function(i) {
	var s = '';

	for(var j = 0; j < i; j++)
	    s = s.concat(String.fromCharCode(a_.rndchar()));

	return s;
    };


    a_.sups = function(s) {
	return '<span style="vertical-align: super; font-size: 85%">' + s + '</span>';
    };


    a_.subs = function(s) {
	return '<span style="vertical-align: sub; font-size: 85%">' + s + '</span>';
    };


    a_.rplc = function(id, text, sw) {
        var iddu = a_.id(id);
        var chistu = document.createElement((sw==1)?"div":"span");
        var patri = iddu.parentNode;

        chistu.setAttribute("id", id);
        patri.replaceChild(chistu, iddu);
        chistu.innerHTML = text;
    };


    a_.subst = function(id, text ) {
	var du = a_.id(id);
	var cs = du.childNodes;
	var i;

	for(i=0; i<cs.length; i++) du.removeChild(cs[i]);

	du.innerHTML = text;
    };


    a_.get = function (url, f) {
	var x = new XMLHttpRequest();

	document.body.style.cursor = 'wait';
	x.open('GET', url);
	x.onreadystatechange = function() {
            if (x.readyState != 4) return;
	    document.body.style.cursor = 'default';
	    f(x.responseText, x.status == 200);
        };
	x.send();
    };


    a_.post = function (url, data, enc, f) {
	var x = new XMLHttpRequest();

	document.body.style.cursor = 'wait';
	x.open('POST', url);
	if (enc) x.setRequestHeader('Content-Type', enc);
	x.onreadystatechange = function() {
            if (x.readyState != 4) return;
	    document.body.style.cursor = 'default';
            f(x.responseText, x.status == 200);
        };
	x.send(data);
    };


    a_.update = function (id) {
	return function (data, ok) {
	    if(ok) a_.subst(id, data);
	};
    };


    a_.esc = function (s) {
	return encodeURIComponent(s).replace(/[!'()*]/g, function(c) {
	    return '%' + c.charCodeAt(0).toString(16);
	});
    };


    a_.unesc = decodeURIComponent;


    return a_;
}();
