			var info_visible = false;
			var maxPics = 25;
            var imgMaxWidth = '800px';
            var imgMaxHeight = '800px';
            
            var START_WITH_ALL = 0;
            var START_WITH_SELECTION = 1;
            var START_RANDOM = 2;
            
            // determina se partire con la visualizzazione di una selezione di immagini
            var startWithSelection = START_RANDOM;
            
            // crea box errore
            var errorAlertBox = document.createElement('div');
            errorAlertBox.id = 'error_box';
            var errorIcon = document.createElement('div');
            errorIcon.id =  "error_icon";
            errorIcon.className = "fa fa-exclamation-triangle fa-4x";
            errorAlertBox.appendChild(errorIcon);
            var errorMessageContainer = document.createElement('span');
            errorMessageContainer.id = 'error_message';
            errorMessageContainer.innerHTML = "Sorry, we couldn't retrieve data.</br>Try again later";
            errorAlertBox.appendChild(errorMessageContainer);
            document.getElementsByTagName('body')[0].appendChild(errorAlertBox);
            
            
            // recupera i dati via AJAX
			var allContribs = [];
            var selectedContribs = []; // selezione di immagini, la prima per ogni tema
            var randomContribs = []; // selezione di immagini random
			var themes = {};
			var themesNames = [];
			var img_ext = ["jpg", "jpeg", "JPG", "JPEG", "png", "PNG", "gif", "GIF", "tif", "TIF", "tiff", "TIFF"];
			var pdf_ext = ["pdf", "PDF"];
			var dataThemes = new XMLHttpRequest();
			dataThemes.onload = themesWait;
			dataThemes.onerror = noDataError;
			dataThemes.onreadystatechange = function() {
				if (dataThemes.readyState == 4 && dataThemes.status != 200) {
				document.getElementById('error_box').style.display = 'block';
				}
			}
			dataThemes.open("get", "/soci/themes", true);
			dataThemes.send();
			
			
			// funzione di callback su errore lettura dati
			function noDataError(e) {
				document.getElementById('error_box').style.display = 'block';
				}

			
			// funzione di callback lettura temi
			function themesWait(e) {
				dataThemes = JSON.parse(this.responseText);
				for (t in dataThemes.message) {
					var theme = dataThemes.message[t]					
					themes[theme] = [];
					}
				var data = new XMLHttpRequest();
				data.onload = dataWait;
				data.onerror = noDataError;
				data.onreadystatechange = function() {
					if (data.readyState == 4 && data.status != 200) {				
					document.getElementById('error_box').style.display = 'block';
					}
				}
				data.open("get", "/soci/contribs", true);
				data.send();				
				}
			
			// funzione di callback per la lettura dei dati
			function dataWait(e) {
			data = JSON.parse(this.responseText);
           
            for (i in data.message.mat) {				
                for (j in data.message.mat[i]) {
                    var d = {};
                    d.url = j;
                    var ext = j.split('.').pop();
                    if (img_ext.indexOf(ext) > -1) {
						d.type = "img";
						}
					else if (pdf_ext.indexOf(ext) > -1) {
						d.type = "pdf";
						}
					else if (j.indexOf('youtube.com') > -1) {
						d.type = "ytvideo";
						d.url = j.split('v=').pop();
						}
					else if (j.indexOf('vimeo.com') > -1) {
						d.type = "vmvideo";
						d.url = j.split('vimeo.com/').pop();
						}
                    d.descr = data.message.mat[i][j].descr;
                    d.themes = data.message.mat[i][j].theme;
                    allContribs.push(d);
                    
                    // crea array tematici
                    for (t in d.themes) {
						if(d.themes[t] in themes) {
							themes[d.themes[t]].push(d);
							}
						}
                }
            }
            
            // crea selezione di contributi pescando il primo per ogni tema
            for (t in  themes) {
                var cSel = themes[t][0];
                // controlla se è un duplicato
                if (!selectedContribs.some(function(e) {return e.url == cSel.url;})) {
                    selectedContribs.push(cSel);
                    };
                }
            
            /* // funzione random farlocca
            var nRandom = Math.min(maxPics, allContribs.length);
            for (var cntrb=0; cntrb < nRandom; cntrb++) {
				var cSel = allContribs[Math.floor((Math.random() * allContribs.length-1) + 1)];
				if (!selectedContribs.some(function(e) {return e.url == cSel.url;})) {
                    selectedContribs.push(cSel);
				}
				}*/
				
			// crea una selezizone casuale di immagini
			var nRandom = Math.min(maxPics, allContribs.length);
			var tempArray = allContribs.slice();
            for (var cntrb=0; cntrb < nRandom; cntrb++) {
				var ind = Math.floor((Math.random() * tempArray.length));
				var cSel = tempArray[ind];
                randomContribs.push(cSel);
                tempArray.splice(ind,1);			
				}
            

			// VAI CON LO SHOW
            if (!startWithSelection) {
                startTheShow(allContribs);
                }
            else { 
				if (startWithSelection == 1) { startTheShow(selectedContribs); }
				else { startTheShow(randomContribs); }
            }
			}
			
            
            // start the show, da chiamare con una tabella di contributi
            function startTheShow(table) {

			var camera, scene, renderer;
			var controls;
            
            var clickTime = 0;

			var objects = [];
			var targets = { sphere: [], helix: [], grid: [] };
			var container = document.getElementById('container');
			var descr_container = document.getElementById('descr_container');
			var players_container = document.getElementById('media_players');
			var info_tags_container = document.getElementById('info');
			var prev_next_container = document.getElementById('prev_next');
			
			var players_w = 640;
			var players_h = 360;
			var player_PDF_height_percent = 0.7;
			
			var req;
                        

            
            
            var pages;
            var activePage = 1;
            var pageLoop = false;
            
            var actualTarget;
            var somethingIsActive = false;
            var autoRotazione = false;

			init();
			animate();

			function init() {

				camera = new THREE.PerspectiveCamera( 40, window.innerWidth / window.innerHeight, 1, 10000 );
				camera.position.z = 3000;

				scene = new THREE.Scene();
                
                pages = Math.ceil(table.length/maxPics);
                
                // inserisce bottoni prev e next se le pagine sono più di una
                if (pages > 1) {
                    //var prev = document.createElement('button');
                    //var next = document.createElement('button');
                    var prev = document.createElement('li');
                    var next = document.createElement('li');
                    prev.className = "fa fa-chevron-left fa-2x menu_enabled";
                    next.className ="fa fa-chevron-right fa-2x menu_enabled";
                    prev.id = 'prev';
                    next.id = 'next';
                    //prev.innerHTML = 'PREV';
                    //next.innerHTML = 'NEXT';
                    
                    prev.addEventListener( 'click', function ( event ) {
                    if (!somethingIsActive) {
                        if (!pageLoop) {
                        if (activePage > 1) {
						switchPage(500);
                        activePage -= 1;
                        enableDisablePrevNext(activePage);
                        //showActivePage();
                        }
                        } else {
                        switchPage(500);
                        if (activePage > 1) {
                        activePage -= 1;
                        }else{activePage = pages;}
                        }
                    }
                    }, false );
                    
                    next.addEventListener( 'click', function ( event ) {
                    if (!somethingIsActive) {
                        if (!pageLoop) {
                        if(activePage < pages) {
						switchPage(500);
                        activePage += 1;
                        enableDisablePrevNext(activePage);
                        //showActivePage();
                        }
                        }else{
                        switchPage(500);
                        if(activePage < pages) {
                        activePage += 1;
                        }else{activePage = 1;}
                        }
                    }
                    }, false );
                    
                    var menu = document.getElementById('prev_next');
                    menu.appendChild(prev);
                    menu.appendChild(next);
                    }
                    
                    
            // prepara menu dei temi
            var info_tags = document.createElement('div');
            info_tags.id = 'tags';
            
            
            var tags_buttons = document.createElement('ul');
            tags_buttons.id = 'tag_buttons';
            var tag_button_all = document.createElement('li');
            tag_button_all.innerHTML = 'all';
            tag_button_all.className = 'tag_button';
            add_changeThemeEvent(tag_button_all, 'all');
            tags_buttons.appendChild(tag_button_all);
            for (k in Object.keys(themes)) {
                var tag_button = document.createElement('li');
                tag_button.innerHTML = Object.keys(themes)[k];
                tag_button.className = 'tag_button';
                add_changeThemeEvent(tag_button, Object.keys(themes)[k]);
                tags_buttons.appendChild(tag_button);
                }
            info_tags.appendChild(tags_buttons);
            var info_tags_tab = document.createElement('li');
            info_tags_tab.id = 'tags_tab';
            //info_tags_tab.className = 'fa fa-tags';
            info_tags_tab.onclick = function () {
                if (info_visible) {info_tags_container.style.left = '-210px';}
                else {info_tags_container.style.left = '0px';}
                info_visible = !info_visible;
                };
            info_tags.appendChild(info_tags_tab);
            info_tags_container.appendChild(info_tags);
            
            // crea menu links in fondo al menu laterale
            var links_ul = document.createElement('ul');
            links_ul.id = "menu_links";
            var link_participate_li = document.createElement('li');
            var link_participate_a = document.createElement('a');
            link_participate_a.href = "/soci";
            link_participate_a.target = "_blank";
            link_participate_a.innerHTML = "PARTICIPATE";
            link_participate_li.appendChild(link_participate_a);
            links_ul.appendChild(link_participate_li);
            info_tags_container.appendChild(links_ul);
                    
                    

				// table - crea elementi

				for ( var i = 0; i < table.length; i += 1 ) {
					
					var og = table[i];
					
					// crea contenitore contributo
					var element = document.createElement( 'div' );
					element.className = 'element';
					
					// crea oggetto THREEJS, 3DObject ruota e 3DSprite rimane sempre rivolto alla camera
					//var object = new THREE.CSS3DSprite( element );
					var object = new THREE.CSS3DObject( element );
					object.position.x = Math.random() * 4000 - 2000;
					object.position.y = Math.random() * 4000 - 2000;
					object.position.z = Math.random() * 4000 - 2000;
                    //object.stored = {"position":{"x":"","y":"","z":""}};
                    //object.stored.position.x = object.position.x;
                    //object.stored.position.y = object.position.y;
                    //object.stored.position.z = object.position.z;
                    object.name = og.url;
                    object.isActive = false;
                    object.n = i; // aggiungiamo all'oggetto il suo ordine nella tavola dati
                    object.page = Math.ceil((i+1)/maxPics);
                    // rende invisibile inizialmente gli oggetti
                    object.visible = false;
                    object.element.style.display = 'none';
                    
                    // crea elemento dom contributo
					if (og.type == "img") {
						var contrib = createImgContrib(og);
					}
					else if (og.type == "ytvideo") {
						var contrib = createYTContrib(og);
					}
					else if (og.type == "vmvideo") {
						var contrib = createVMContrib(og);
					}
					else if (og.type == "pdf") {
						var contrib = createPdfContrib(og);
					}
					contrib.id = "contrib_"+object.n;
					element.appendChild( contrib );
                    
                    // crea descrizione
					var descr = document.createElement('div');
					descr.className = 'descr';
					descr.id = 'descr_'+i;
					var descr_p = document.createElement('p');
					descr_p.className = 'descr_p';
					descr_p.innerHTML= og.descr;
					descr.appendChild(descr_p);
					// aggiunge tags
					var descr_tags = document.createElement('ul');
					descr_tags.className = 'tags';
					for (tg in og.themes) {
						var tag = document.createElement('li');
						tag.className = 'tag';
						tag.innerHTML = og.themes[tg];
						add_changeThemeEvent(tag, og.themes[tg]);
						descr_tags.appendChild(tag);
						}
					descr.appendChild(descr_tags);
					
					descr_container.appendChild(descr);
					
					element.parent = object;
					
										
					//crea video e pdf player
					// youtube and vimeo
					if (og.type == "ytvideo" || og.type == "vmvideo") {
						var player_type = og.type;
						var player_div = document.createElement('div');
						player_div.className = 'player_div';
						player_div.id = 'player_div_'+i;
						var player = document.createElement('iframe');
						player.className = 'player ' + player_type;
						player.id = 'player_'+i;
						if (player_type == "ytvideo") {
							player.src = 'https://www.youtube.com/embed/'+og.url+'?enablejsapi=1';
						}
						else {
							player.src = 'http://player.vimeo.com/video/'+og.url+'?api=1';
						}
						player.width = players_w;
						player.height = players_h;						
						player_div.appendChild(player)
						var close_btn = document.createElement('li');
						close_btn.id = 'player_'+i+'_close';
                        close_btn.className = 'player_close_btn fa fa-times-circle-o fa-2x';
						//close_btn.innerHTML = 'close';
						add_close_player_event(close_btn, element, player_div, player, player_type);					
						player_div.appendChild(close_btn);
						players_container.appendChild(player_div);
						}
					// PDF
					else if (og.type == "pdf") {
						var player_type = og.type;
						var player_div = document.createElement('div');
						player_div.className = 'player_div';
						player_div.id = 'player_div_'+i;
						var player = document.createElement('object');
						player.data = og.url;
						player.type = 'application/pdf';				
						player.width = players_w;
						//player.height = players_w * 1.0;
						player.height = (window.innerHeight || document.documentElement.clientHeight || document.getElementsByTagName('body')[0].clientHeight)*player_PDF_height_percent;	
						player.className = 'player ' + player_type;
						player.id = 'player_'+i;
						var pdf_disclaimer = document.createElement('p');
						pdf_disclaimer.innerHTML = "It appears you don't have a PDF plugin for this browser.</br>You can";
						pdf_link = document.createElement('a');
						pdf_link.href = og.url;
						pdf_link.innerHTML = "click here to download the PDF file";
						pdf_link.target = "_blank";
						pdf_disclaimer.appendChild(pdf_link);
						player.appendChild(pdf_disclaimer);					
						player_div.appendChild(player)
						var close_btn = document.createElement('li');
						close_btn.id = 'player_'+i+'_close';
                        close_btn.className = 'player_close_btn fa fa-times-circle-o fa-2x';
						add_close_player_event(close_btn, element, player_div, player, player_type);					
						player_div.appendChild(close_btn);
						players_container.appendChild(player_div);						
						}
						                    
                    
                    // rende cliccabili gli elementi, con doppio click
                    //element.parent = object;
                    
                    object.element.onmousedown = function() {
                        clickTime = new Date().getTime();
                        }
                    
                    
                    object.element.onmouseup = function() {
                        if (new Date().getTime() - clickTime < 300) {
                        if (!this.parent.isActive && !somethingIsActive) {
                            somethingIsActive = true;
                            this.parent.isActive = true;
                            
                            if (hasClass(this.firstChild, 'img_contrib')) {
								// mette al centro
                                moveToCenter(this.parent.n, 300); // sostituisce i passaggi sopra
								this.firstChild.style.opacity = '1.0';
							}
							else if (hasClass(this.firstChild, 'yt_contrib') || hasClass(this.firstChild, 'vm_contrib') || hasClass(this.firstChild, 'pdf_contrib')) {
								// mostra il player
								document.getElementById('player_div_'+this.parent.n).style.display = 'block';
								this.firstChild.style.opacity = '0.1';							
								}
							controls.autoRotate = false;
                            document.getElementById('descr_'+this.parent.n).style.visibility = 'visible';
                            document.getElementById('descr_container').style.right = '220px';
                        
                            } else if (this.parent.isActive && somethingIsActive) {
								if (hasClass(this.firstChild, 'img_contrib')) {
                                    document.getElementById('descr_container').style.right = '0px';
									document.getElementById('descr_'+this.parent.n).style.visibility = 'hidden';
                                    transformSingle(actualTarget, this.parent.n, 500); // riposizioniamo solo oggetto attivo
									somethingIsActive = false;
									this.parent.isActive = false;
									this.firstChild.style.opacity = '0.75';
									controls.autoRotate = autoRotazione;
								}
                            }                        
                        }
                        };
                     
                     // vedere se mantenere questi eventi   
                     object.element.onmouseover = function() {
                        if (!this.parent.isActive && !somethingIsActive) {
                            //this.parent.position.z -= 10;
                            this.parent.translateOnAxis(new THREE.Vector3(0,0,1), 50);
                            render();
                            }                        
                        };
                        
                     object.element.onmouseout = function() {
                        if (!this.parent.isActive && !somethingIsActive) {
                            //this.parent.position.z += 10;
                            this.parent.translateOnAxis(new THREE.Vector3(0,0,1), -50);
                            render();
                           
                            }                        
                        };          
                           
					// aggiunge oggetto alla scena THREEJS e all'array degli oggetti

					scene.add( object );
					objects.push( object );
				}

				// sfera

				var vector = new THREE.Vector3();

				for ( var i = 0, l = objects.length; i < l; i ++ ) {
                    
                    var j = i%maxPics;

					var phi = Math.acos( -1 + ( 2 * j ) / (l%maxPics) );
					var theta = Math.sqrt( l * Math.PI ) * phi;

					var object = new THREE.Object3D();

					object.position.x = 1000 * Math.cos( theta ) * Math.sin( phi );
					object.position.y = 1000 * Math.sin( theta ) * Math.sin( phi );
					object.position.z = 1000 * Math.cos( phi );

					vector.copy( object.position ).multiplyScalar( -2 );

					object.lookAt( vector );

					targets.sphere.push( object );

				}

				// spirale

				var vector = new THREE.Vector3();
                var variation = 100; 
                var radius = 5000; // 1500
                var jump = 40; // 20

				for ( var i = 0, l = objects.length; i < l; i ++ ) {

					var j = i%maxPics;
                    //var phi = i * 0.35 + Math.PI;
                    //var phi = j * 0.55 + Math.PI;
                    var phi = j * 0.5 + Math.PI;

					var object = new THREE.Object3D();
                    
                    object.position.x = 2500 * Math.sin( phi ) 
                    if (j % 2 === 0) {
                        object.position.y = +(( j * jump ) + variation);
                    } else {
                        object.position.y = -(( j * jump ) + variation);
                        }
					object.position.z = radius * Math.cos( phi );

					vector.x = object.position.x * 2;
					vector.y = object.position.y;
					vector.z = object.position.z * 2;

					object.lookAt( vector.multiplyScalar(-1) );

					targets.helix.push( object );

				}

				// griglia

				for ( var i = 0; i < objects.length; i ++ ) {

					var object = new THREE.Object3D();

					object.position.x = ( ( i % 5 ) * 400 ) - 800;
					object.position.y = ( - ( Math.floor( i / 5 ) % 5 ) * 400 ) + 800;
					object.position.z = ( Math.floor( i / 25 ) ) * 1000 - 2000;

					targets.grid.push( object );

				}

				// renderer THREEJS CSS3D

				renderer = new THREE.CSS3DRenderer();
				renderer.setSize( window.innerWidth, window.innerHeight );
				renderer.domElement.style.position = 'absolute';
				document.getElementById( 'container' ).appendChild( renderer.domElement );

				// controlli movimento vortice
				// trackball
				/*
				controls = new THREE.TrackballControls( camera, renderer.domElement );
				controls.rotateSpeed = 1.0;
				controls.minDistance = 250;
				controls.maxDistance = 980;
                //controls.staticMoving = true;
                controls.autoRotSpeed = 0.1;
                controls.autoRotate = autoRotazione;
                controls.noPan = true;
                //controls.maxVerticalDeviation = 0.8;
				controls.addEventListener( 'change', render );
				*/
				
				// orbit
				controls = new THREE.OrbitControls( camera, renderer.domElement );
				controls.rotateSpeed = -0.2;
				controls.enableDamping = true;
				controls.dampingFactor = 0.25;
				controls.enableZoom = true;
				controls.autoRotate = autoRotazione;
				controls.autoRotateSpeed = 0.075;	
				controls.minDistance = 250;
				controls.maxDistance = 980;
				controls.minPolarAngle = Math.PI/4; // radians
				controls.maxPolarAngle = Math.PI*3/4; // radians
                //controls.staticMoving = true;
                controls.enablePan = false;
				controls.addEventListener( 'change', render );
				
				
                
                // rende visibili inizialmente gli oggetti della pagina 1
                showActivePage();
                if (pages > 1 && !pageLoop) {
                    changeClass(document.getElementById("prev"), "menu_enabled", "menu_disabled");
                    }
                
                
                // configurazione target iniziale
                actualTarget = targets.helix;
				transform( targets.helix, 2000 );

				window.addEventListener( 'resize', onWindowResize, false );

			} // fine init
					
			
			// FUNZIONI
            
            // cambia classe di un oggetto
            function changeClass(object,oldClass,newClass)
            {
                // replace:
                var regExp = new RegExp('(?:^|\\s)' + oldClass + '(?!\\S)', 'g');
                object.className = object.className.replace( regExp , " "+newClass );
            }

            function removeClass(object,oldClass)
            {
                // remove:
                object.className = object.className.replace( /(?:^|\s)oldClass(?!\S)/g , ' ' );
            }
            
            function addClass(object,newClass)
            {
                // add
                object.className += " "+newClass;
            }
            
            // mostra gli oggetti della pagina attiva
            function showActivePage() {                
                for (var i = 0; i < objects.length; i ++) {
                    var o = objects[i];
                    if (o.page == activePage) {
                            o.visible = true;
                            o.element.style.display = 'block';
                        } else {
                            o.visible = false;
                            o.element.style.display = 'none';
                            }
                    }
                }
                
            // mostra gli oggetti di una pagina data
            function showPage(n) {                
                for (var i = 0; i < objects.length; i ++) {
                    var o = objects[i];
                    if (o.page == n) {
                            o.visible = true;
                            o.element.style.display = 'block';
                        } else {
                            o.visible = false;
                            o.element.style.display = 'none';
                            }
                    }
                }
                
             // cambia tag
             function changeTheme(theme) {
				 // fare pulizia e poi ricominciare lo show
                var duration = 200;
                TWEEN.removeAll();
				var counter = 0;
				for ( var i = 0; i < objects.length; i ++ ) {
					var object = objects[ i ];					
					if (object.page == activePage) {
					counter +=1;
                    
					new TWEEN.Tween( object.position )
						.to( { x: object.position.x, y: "-2500", z: object.position.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
                        .onUpdate( render )                        
                        .onComplete(function(){ counter -=1; if (counter<1) {
                            setTimeout( function() {
                            cancelAnimationFrame(req);
                            TWEEN.removeAll();
                            scene = null;
                            projector = null;
                            camera = null;
                            controls = null;
                            empty(container);
                            empty(descr_container);
                            empty(players_container);
                            empty(info_tags_container);
                            empty(prev_next_container);
                            if (theme == 'all') {startTheShow(allContribs);}
                            else {startTheShow(themes[theme]);}
                            }, Math.random() * duration/2 + duration/2);
                            }                            
                            })
						.start();
                        
                    new TWEEN.Tween( this )
						.to( {}, duration * 2 )
						.onUpdate( render )
						.start();
                    }
				}				 
            }
			 
					
			// ripulisce elementi DOM
			function empty(elem) {
				while (elem.lastChild) elem.removeChild(elem.lastChild);
			}
                
            // abilita-disabilita pulsanti prev-next
            function enableDisablePrevNext(activePage) {
                    if (activePage == 1) {changeClass(document.getElementById("prev"), "menu_enabled", "menu_disabled");}
                    else {changeClass(document.getElementById("prev"), "menu_disabled", "menu_enabled");}
                    if (activePage == pages) {changeClass(document.getElementById("next"), "menu_enabled", "menu_disabled");}
                    else {changeClass(document.getElementById("next"), "menu_disabled", "menu_enabled");}
                }
            
			
			// controlla se un elemento ha una certa classe, questione di stile
			function hasClass(element, cls) {
				return (' ' + element.className + ' ').indexOf(' ' + cls + ' ') > -1;
			}
			
			// aggiunge event handler ai bottoni per la chiusura dei player
			function add_close_player_event(b, el, p_div, p, t) {
				 b.addEventListener('click', function(e) {
					p_div.style.display = 'none';
					if (t == "ytvideo") {
						p.contentWindow.postMessage('{"event":"command","func":"pauseVideo","args":""}', '*');
						}
					else if (t == "vmvideo") {
						p.contentWindow.postMessage('{"method":"pause"}', '*');
						}
					somethingIsActive = false;
					el.parent.isActive = false;
                    document.getElementById('descr_container').style.right = '0px';
					document.getElementById('descr_'+el.parent.n).style.visibility = 'hidden';
					document.getElementById('contrib_'+el.parent.n).style.opacity = '0.75';
					controls.autoRotate = autoRotazione;					
				}, false);					
				}
				
			// aggiunge event handler ai tag
			function add_changeThemeEvent(t, tn) {
				t.addEventListener('click', function(e) {
					changeTheme(tn);
				}, false);
				}
				
			
			// funzioni di creazione elementi DOM contributi
			function createImgContrib (og) {
				var c =document.createElement( 'img' );
					c.className = 'contrib img_contrib';
					c.src = og.url;
                    c.style.backgroundColor = '#fff';
                    c.style.padding = '8px';
                    c.style.width = '100%';
                    c.style.maxWidth = imgMaxWidth;
                    c.style.height = '100%';
                    c.style.maxHeight = imgMaxHeight;
                    return c;			
			}
			
			function createPdfContrib (og) {
				var c =document.createElement( 'img' );
					c.className = 'contrib pdf_contrib';
					c.src = og.url.substr(0, og.url.lastIndexOf(".")) + ".png";
                    c.style.backgroundColor = '#fff';
                    c.style.padding = '8px';
                    c.style.width = '100%';
                    c.style.maxWidth = imgMaxWidth;
                    c.style.height = '100%';
                    c.style.maxHeight = imgMaxHeight;
                    return c;			
			}
			
			function createYTContrib (og) {
				var c =document.createElement( 'img' );
					c.className = 'contrib yt_contrib';
					c.src = "http://img.youtube.com/vi/"+og.url+"/0.jpg";
                    c.style.backgroundColor = '#fff';
                    c.style.padding = '8px';
                    c.style.width = '100%';
                    c.style.maxWidth = imgMaxWidth;
                    c.style.height = '100%';
                    c.style.maxHeight = imgMaxHeight;
                    return c;				
				}
				
			function createVMContrib (og) {
				var c =document.createElement( 'img' );
					c.className = 'contrib vm_contrib';
					var d = new XMLHttpRequest();
					d.onload = check_thumbnail	;
					d.open("get", "http://vimeo.com/api/v2/video/"+og.url+".json", true);
					d.send();
					// funzione di callback per la lettura dei dati
					function check_thumbnail(e) {				
						var t = JSON.parse(this.responseText);
						c.src = t[0].thumbnail_large;
						c.style.backgroundColor = '#fff';
						c.style.padding = '8px';
                        c.style.width = '100%';
                        c.style.maxWidth = imgMaxWidth;
                        c.style.height = '100%';
                        c.style.maxHeight = imgMaxHeight;						
					}
					return c;					
				}

			function transform( targets, duration ) {

				TWEEN.removeAll();
				for ( var i = 0; i < objects.length; i ++ ) {
					var object = objects[ i ];
					var target = targets[ i ];
					new TWEEN.Tween( object.position )
						.to( { x: target.position.x, y: target.position.y, z: target.position.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.start();
					new TWEEN.Tween( object.rotation )
						.to( { x: target.rotation.x, y: target.rotation.y, z: target.rotation.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.start();
				}
				new TWEEN.Tween( this )
					.to( {}, duration * 2 )
					.onUpdate( render )
					.start();
			}
            
            function transformSingle( targets, id, duration ) {
				TWEEN.removeAll();
                var object = objects[ id ];
                var target = targets[ id ];
                new TWEEN.Tween( object.position )
						.to( { x: target.position.x, y: target.position.y, z: target.position.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.start();
                new TWEEN.Tween( object.rotation )
						.to( { x: target.rotation.x, y: target.rotation.y, z: target.rotation.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.start();				
				new TWEEN.Tween( this )
					.to( {}, duration * 2 )
					.onUpdate( render )
					.start();
			}
            
            function moveToCenter( id, duration ) {
				TWEEN.removeAll();
                var object = objects[ id ];
                new TWEEN.Tween( object.position )
						.to( { x: "0", y: "0", z: "0" }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.start();
                new TWEEN.Tween( object.rotation )
						.to( { x: camera.rotation.x, y: camera.rotation.y, z: camera.rotation.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.start();				
				new TWEEN.Tween( this )
					.to( {}, duration*2 )
					.onUpdate( render ).onComplete(function () {
                        object.up = camera.up;
                        object.lookAt(camera.position);
                        })
					.start();
			}
			
			
			function switchPage(duration ) {
				TWEEN.removeAll();
				var counter = 0;
				for ( var i = 0; i < objects.length; i ++ ) {
					var object = objects[ i ];
					
					if (object.page == activePage) {
					counter +=1;
					new TWEEN.Tween( object.position )
						.to( { x: object.position.x, y: "-2500", z: object.position.z }, Math.random() * duration + duration )
						.easing( TWEEN.Easing.Exponential.InOut )
						.onComplete(function(){counter -=1; if (counter<1) {
							showActivePage();
							transform(actualTarget, 500);}})
						.start();
				
					new TWEEN.Tween( this )
						.to( {}, duration * 2 )
						.onUpdate( render )
						.start();
				}
				else object.position.y = 2500;
				}
			}

			function onWindowResize() {

				camera.aspect = window.innerWidth / window.innerHeight;
				camera.updateProjectionMatrix();

				renderer.setSize( window.innerWidth, window.innerHeight );

				render();

			}

			function animate() {

				req = requestAnimationFrame( animate );

				TWEEN.update();

				controls.update();

			}

			function render() {

				renderer.render( scene, camera );

			}
			
			function detectIE() {
				var ua = window.navigator.userAgent;	
				var msie = ua.indexOf('MSIE ');
				if (msie > 0) {
					// IE 10 or older => return version number
					return parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
				}
				var trident = ua.indexOf('Trident/');
				if (trident > 0) {
					// IE 11 => return version number
					var rv = ua.indexOf('rv:');
					return parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
				}
				var edge = ua.indexOf('Edge/');
				if (edge > 0) {
					// IE 12 => return version number
					return parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
				}
				// other browser
				return false;
			}

}
