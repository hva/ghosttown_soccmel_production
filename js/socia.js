var socia = function () {
    /* ad hoc */

    var f = {};

    f.dologin = function(id) {
	_.subst(id,
		'<input autofocus type="text" class="form-control" id="luser" placeholder="username">&nbsp;' +
		'<input id="lpass" type="password" class="form-control" placeholder="password" onkeyup="if(_.kid.ret) socia.login' +
		qargs('luser', 'lpass', 'lnope') + '">&nbsp;' +
		'<button class="form-control btn btn-default" onclick="socia.login' + qargs('luser', 'lpass', 'lnope') + '">login</button>&nbsp;' +
		'<span id="lnope" class="warn"></span>');
    };

    f.doregister = function(id) {
	var txt = _.esc(_.id(id).innerHTML);

	_.subst(id,
		'<table><tr><td><input autofocus id="ruser" type="text" placeholder="username"></td>' +
		'<td><input id="email" type="text" name="email" placeholder="email address"></td></tr>' +
		'<tr><td><input id="rpass" type="password" placeholder="password"></td>' +
		'<td><input id="veri" type="password" placeholder="double check it" ' +
		'onkeyup="if(_.kid.ret) socia.register' + qargs('ruser', 'rpass', 'veri', 'email', 'rnope', id, txt) + '"></td>' +
		'</td></tr></table><button onclick="socia.register' + qargs('ruser', 'rpass', 'veri', 'email', 'rnope', id, txt) +
		'">register</button>' +
		'&nbsp;<span id="rnope" class="warn"></span>');
    };

    f.dochangepwd = function(usr, id) {
	var txt = _.esc(_.id(id).innerHTML);

	_.subst(id,
		'&nbsp;<input id="opass" type="password" placeholder="old password">' +
		'&nbsp;<input id="npass" type="password" placeholder="new password">' +
		'&nbsp;<input id="veri" type="password" placeholder="check it" ' +
		'onkeyup="if(_.kid.ret) socia.changepwd' +
		qargs(usr, 'opass', 'npass', 'veri', 'nope', id, txt) + '">' +
		'&nbsp;<span id="nope" class="warn"></span>');
    };


    f.login = function (user, pass, w) {
	var p = _.id(pass).value || '';
	var u = _.id(user).value || '';

	p = CryptoJS.SHA3(p, {outputLength: 256}).toString(CryptoJS.enc.Hex);
	_.post('auth', '{"user": "' +  u + '", "password": "' + p + '"}', 'application/json;charset=UTF-8', function(data, ok) {
	    var resp = JSON.parse(data);
	    if(!ok)_.subst(w, 'connection failed, try again');
	    else if(resp.err) _.subst(w, resp.message);
	    else window.location = resp.message;
	});
    };


    f.gohome = function (u) {
	var hidden = document.createElement('input');
	var form = document.createElement('form');

	hidden.type = 'hidden';
	hidden.name = 'usr';
	hidden.value = u;
	form.action = 'home';
	form.method = 'post';
	form.appendChild(hidden);
	document.getElementsByTagName('body')[0].appendChild(form);
	form.submit();
    };


    f.register = function (user, pass, veri, email, w, id, txt) {
	var p = _.id(pass).value || '';
	var v = _.id(veri).value || '';
	var u = _.id(user).value || '';
	var e = _.id(email).value || '';

	if(!u.length) {
	    _.subst(w,'please choose a username');
	    return;
	}
	if(!e.length) {
	    _.subst(w,'please write your email address');
	    return;
	}
	if(!p.length || p !== v) {
	    _.subst(w, 'passwords do not match');
	    return;
	}
	p = CryptoJS.SHA3(p, {outputLength: 256}).toString(CryptoJS.enc.Hex);
	_.post('newu', '{"user": "' +  u + '", "password": "' + p + '", "email": "' + e + '"}',
	       'application/json;charset=UTF-8', function(data, ok) {
		   if(!ok) _.subst(w, 'connection failed, try again');
		   else {
		       var resp = JSON.parse(data);

		       if(resp.err) _.subst(w, resp.message);
		       else _.subst(id, _.unesc(txt) + '<br><span class="warn">' + resp.message + '</span>');
		   }
	});
    };


    f.changepwd = function (usr, opass, npass, veri, w, id, txt) {
	var o = _.id(opass).value || '';
	var n = _.id(npass).value || '';
	var v = _.id(veri).value || '';

	if(!n.length || n !== v) {
	    _.subst(w, 'new passwords do not match');
	    return;
	}
	o = CryptoJS.SHA3(o, {outputLength: 256}).toString(CryptoJS.enc.Hex);
	n = CryptoJS.SHA3(n, {outputLength: 256}).toString(CryptoJS.enc.Hex);
	_.post('password/' + usr, '{"old": "' + o  + '", "new": "' + n + '"}',
	       'application/json;charset=UTF-8', function(data, ok) {
		   if(!ok) _.subst(w, 'connection failed, try again');
		   else {
		       var resp = JSON.parse(data);

		       if(resp.err) {
			   if(resp.err == 1) window.location = resp.message;
			   else _.subst(w, resp.message);
		       } else _.subst(id, _.unesc(txt) + '<span class="warn">&nbsp;(just changed)</span>');
		   }
	       });
    };


    f.upload = function (usr, fid, did, mid, bid) {
	if(!_.id(fid).value || !_.id(did).value) return;
	var warn = invalidcontr(_.id(fid).value);

	if(warn) {
	    _.subst(mid, warn);
	    return;
	}
	var form = new FormData();

	form.append('descr', _.id(did).value);
	form.append('file', _.id(fid).files[0]);
	_.subst(mid, '&nbsp;uploading file...');
	_.post('matUpload/' + usr, form, null, function (data, ok) {
	    if(ok) {
		var resp = JSON.parse(data);

		if(resp.err) {
		    if(resp.err == 1) window.location = resp.message;
		    else {
			_.subst(mid, resp.message);
			_.id(fid).value = null;
			_.id(did).value = null;
		    }
		} else {
		    _.subst(mid, '');
		    _.id(fid).value = null;
		    _.id(did).value = null;
		    f.matlist(usr, bid);
		}
	    }
	    else _.subst(mid, 'upload failed, please retry');
	});
    };


    f.matdel = function (usr, file, descr, bid) {
	_.post('matDelete/' + usr, '{"file": "' + file + '", "descr": "' + descr + '"}',
	       'application/json;charset=UTF-8', function (data) {
		   var resp = JSON.parse(data);

		   if(resp.err) window.location = resp.message;
		   else f.matlist(usr, bid);
	       });
    };


    f.descr = function (usr, url, id, txt, bid) {
	_.subst(id, '<input id="txt'  + id + '" type="text" size="40" onkeyup="if(_.kid.ret) socia.matdescr' +
		qargs(id, bid) + '" value="' + _.unesc(txt).replace(/"/g, '&#34;') +
		'"><input id="usr'  + id + '" type="hidden" value="' + usr +
		'"><input id="url'  + id + '" type="hidden" value="' + url +'">');
    };


    f.matdescr = function (id, bid) {
	_.post('matDescr/' + _.id('usr' + id).value, '{"descr": "' + _.esc(_.id('txt' + id).value) +
	       '", "url": "' + _.id('url' + id).value + '"}', 'application/json;charset=UTF-8', function (data) {
		   var resp = JSON.parse(data);

		   if(resp.err) window.location = resp.message;
		   else f.matlist(_.id('usr' + id).value, bid);
	       });
    };


    f.urload = function (usr, uid, did, bid) {
	if(!_.id(uid).value || !_.id(did).value) return;
	_.post('urlUpload/' + usr, '{"descr": "' + _.esc(_.id(did).value) + '", "url": "' + _.id(uid).value +
	       '"}', 'application/json;charset=UTF-8', function (data) {
		   var resp = JSON.parse(data);

		   if(resp.err) {
		       if(resp.err == 1) window.location = JSON.parse(data).message;
		   }
		   else f.matlist(usr, bid);
		   _.id(uid).value = null;
		   _.id(did).value = null;
	       });
    };


    f.unpublish = function (usr, url, descr, pid) {
	_.post('unpublish/' + usr, '{"url": "' + url + '", "descr": "' + descr + '"}', 'application/json;charset=UTF-8',
	       function (data, ok) {
		   if(ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else {
			   _.subst('but' + pid, pubutton(0, qargs(usr, url, descr, pid)));
			   _.subst('own' + pid, '');
			   _.subst('glo' + pid, '');
		       }
		   }
	       });
    };


    f.publish = function (usr, url, descr, pid) {
	_.get('themes',
	      function (data, ok) {
		  _.subst('but' + pid, '');
		  _.subst('own' + pid, '');
		  _.subst('glo' + pid, glothemes(url, usr, descr, pid, (ok)?JSON.parse(data).message:[]));
	      });
    };


    f.addgloth = function (usr, url, descr, pid, list) {
	_.get('themes',
	      function (data, ok) {
		  _.subst('bown' + pid, '');
		  _.subst('glo' + pid, glothemes(url, usr, descr, pid, (ok)?xar(JSON.parse(data).message, list):[]));
	      });
    };


    f.themeadd = function (usr, url, descr, pid, vid) {
	_.post('themeAdd/' + usr, '{"url": "' + url + '", "descr": "' + descr + '", "theme": "' + _.id(vid).value + '"}',
	       'application/json;charset=UTF-8', function (data, ok) {
		   if (!ok) {
		       _.subst('own' + pid, '');
		       return;
		   }
		   var resp = JSON.parse(data);

		   if(resp.err) window.location = resp.message;
		   else {
		       _.subst('but' + pid, pubutton(1, qargs(usr, url, descr, pid)));
		       _.subst('own' + pid, ownthemes(url, usr, descr, pid, resp.message));
		       _.subst('glo' + pid, '');
		   }
	       });
    };


    f.themedel = function (usr, url, descr, pid) {
	_.post('themeDel/' + usr, '{"url": "' + url + '", "descr": "' + descr + '", "theme": "' + _.id('sown' + pid).value + '"}',
	       'application/json;charset=UTF-8',
	       function (data, ok) {
		   if (!ok) {
		       _.subst('own' + pid, '');
		       return;
		   }
		   var resp = JSON.parse(data);

		   if(resp.err) window.location = resp.message;
		   else {
		       if(resp.message.length) _.subst('own' + pid, ownthemes(url, usr, descr, pid, resp.message));
		       else {
			   _.subst('but' + pid, pubutton(0, qargs(usr, url, descr, pid)));
			   _.subst('own' + pid, '');
		       }
		       _.subst('glo' + pid, '');
		   }
	       });
    };


    f.doprivate = function (uid, usr, id) {
	f.matlist(usr, id);
    };


    f.matlist = function (usr, id) {
	_.get('themes', function (data, ok) {
	    if(ok) {
		_.get('matList/' + usr, function (dat, yes) {
		    if(yes) {
			var r = JSON.parse(dat).message;

			if(r.length) {
			    var i, s = '';

			    for(i = 0; i<r.length; i++) {
				var url = r[i].url, txt = r[i].descr, descr = _.esc(txt), themes = r[i].theme, pub = themes.length;
				var did = Math.random().toString(36).substr(2,7), pid = Math.random().toString(36).substr(2,7);

				s += '<br>' +
				    '<div class="small">' +
				    '<span>' + new Date(r[i].tstamp).toUTCString() +
				    '</span>' +
				    '<br>' +
				    '<button onclick="socia.matdel' + qargs(usr, url, descr, id) + '">remove' +
				    '</button>' +
				    '<span id="but' + pid + '">&nbsp;' + pubutton(pub, qargs(usr, url, descr, pid)) +
				    '</span>' +
				    '<span class="redp bold" onclick="window.open' + qargs(url) + '">&nbsp;' +
				    ((url.indexOf('http')) ? url.slice(url.lastIndexOf('/') + 1) : url) +
				    '</span>' +
				    '<span id="' + did + '">' +
				    '<span class="redp" onclick="socia.descr' +
				    qargs(usr, url, did, descr, id) + '">&nbsp;' + txt +
				    '</span>' +
				    '</span>' +
				    '<div>' +
				    '<span id="own' + pid + '">' + ((pub)?ownthemes(url, usr, descr, pid, themes):'') +
				    '</span>' +
				    '<span id="glo' + pid + '">' +
				    '</span>' +
				    '</div>' +
				    '</div>';
			    }
			    _.subst(id, s);
			}
			else _.subst(id,'<span style="color: grey; font-size: 80%">(nothing)</span>');
		    }
		});
	    }
	});
    };


    f.initcontribs = function (id) {
	var i, t, sel = _.id(id);

	_.get('contribs', function(data) {
	    contribs = JSON.parse(data).message;
	    t = Object.keys(contribs.theme).sort();
	    for(i = 0; i<t.length; i++) {
		var x = document.createElement('option');

		x.value = x.text = t[i];
		sel.add(x);
	    }
	});
    };


    f.showcontribs = function (sid, lid) {
	var i, theme = _.id(sid).value, mat = contribs.theme[theme], s = '<table><tr><td>&nbsp;</td></tr>';

	for(i = 0; i< mat.length; i++) {
	    var j, ref = contribs.mat[mat[i].usr][mat[i].url], link = ref.theme.filter(function (e) {return (e!=theme);});

	    s += '<tr><td>on ' + new Date(ref.tstamp).toUTCString() + ', </td>' +
		'<td>by ' + mat[i].usr + ': </td>' +
		'<td><span class="redp" onclick="window.open' + qargs(mat[i].url) + '">' +
		((ref.descr.trim().length)?ref.descr:'document with no description') + '</span></td></tr>';
	    if(link.length) {
		s += '<tr><td colspan="4">related to ' +
		    '<span class="redp" onclick="_.id' + qargs(sid) + '.value=\'' + link[0] + '\'; socia.showcontribs' +
		    qargs(sid, lid) + '">' + link[0] + '</span>';
		for(j = 1; j<link.length; j++)
		    s += ', <span class="redp" onclick="_.id' + qargs(sid) + '.value=\'' + link[j] + '\'; socia.showcontribs' +
		    qargs(sid, lid) + '">' + link[j] + '</span>';
	    }
	    s += '<tr><td>&nbsp;</td></tr>';
	}
	_.subst(lid, s + '</table>');
    };


    f.initadmin = function (usr, lid, nid) {
	_.get('usersAndNotification/' + usr, function(data, ok) {
	    if (ok) {
		var resp = JSON.parse(data);

		if(resp.err) window.location = resp.message;
		else {
		    var x = document.createElement('option');
		    var i, t, sel = _.id(lid);

		    x.text = 'please choose a user';
		    sel.add(x);
		    t = resp.message.usr.sort();
		    for(i = 0; i<t.length; i++) {
			x = document.createElement('option');

			x.value = x.text = t[i];
			sel.add(x);
		    }
		    _.id(nid).checked = resp.message.isNotified;
		}
	    }
	});
    };

    f.showuser = function (usr, lid, uid) {
	_.post('contribs/' + usr, '{"usr": "' + _.id(lid).value + '"}',
	       'application/json;charset=UTF-8',
	       function (data, ok) {
		   if (ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else {
			   var rec = resp.message;
			   var u = rec.usr;
			   var mat = rec.mat;
			   var i, oid, url;

			   var s = '<br><input id="pass" type="password" onkeyup="if(_.kid.ret) socia.doforcepwd' +
			       qargs('pass', 'nope', u, usr, lid, uid) + '" placeholder="force password">' +
			       '&nbsp;&nbsp;<span id="nope" class="warn"></span><br>' +
			       '<br><input id="email" type="text" onkeyup="if(_.kid.ret) socia.doforceemail' +
			       qargs('email', 'enope', u, usr, lid, uid) + '" placeholder="force email address">' +
			       '&nbsp;&nbsp;<span id="enope" class="warn">now ' + rec.email + '</span><br>' +
			       '<br>' + u + ':<br>' +
			       '<input id="blogs" type="checkbox" onchange="socia.role' + qargs('blogs', u, usr, lid, uid) +
			       '"' + ((rec.blogs)?' checked':' ') + '>&nbsp;blogs<br>' +
			       '<input id="comments" type="checkbox" onchange="socia.role' + qargs('comments', u, usr, lid, uid) +
			       '"' + ((rec.comments)?' checked':' ') + '>&nbsp;comments<br>' +
			       '<input id="contributes" type="checkbox" onchange="socia.role' + qargs('contributes', u, usr, lid, uid) +
			       '"' + ((rec.contributes)?' checked':' ') + '>&nbsp;contributes<br>' +
			       '<input id="isBofh" type="checkbox" onchange="socia.role' + qargs('isBofh', u, usr, lid, uid) +
			       '"' + ((rec.isBofh)?' checked':' ') + '>&nbsp;administers<br>';
			   if(mat.length) {
			       s+= '<br>and is contributing with:<br><div class="small"><table class="c-table">';
			       for(i = 0; i<mat.length; i++) {
				   url = mat[i].url;
				   oid = Math.random().toString(36).substr(2,7);
				   s += '<tr><td><span id="b' + oid + '"><button onclick="socia.doforceunpublish' +
				       qargs(oid, u, usr, lid, uid) + '">suspend publication</button></span></td>' +
				       '<td><input type="hidden" id="' + oid + '" value="' + url + '"><span class="bold">' +
				       ((url.slice(0, 4)=='http')?url:url.slice(url.lastIndexOf('/') + 1)) + '</span>' +
				       '&nbsp;&nbsp;<span id="d' + oid + '">' + mat[i].descr + '</span></td></tr>';
			       }
			       s += '</table></div>';
			   }

			   _.subst(uid, s);
		       }
		   }
	       });
    };


    f.notification = function(usr, notified) {
	_.post('notification/' + usr, '{"isNotified": ' + ((_.id(notified).checked)?1:0) + '}',
	       'application/json;charset=UTF-8',
	       function (data, ok) {
		   if (ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else _.id(notified).checked = resp.message.isNotified;
		   }
		   else _.id(notified).checked = _.id(notified).checked?0:1;
	       });
    };


    f.doforcepwd = function(pass, nope, u, usr, lid, uid) {
	var pwd = _.id(pass).value;
	_.subst(nope, 'you are changing by force the password of ' + u + ' to ' + pwd +
		'&nbsp;&nbsp;<button onclick="socia.forcepwd' +	qargs(pwd, u, usr, lid, uid) + '">I know, go on</button>');
    };


    f.forcepwd = function(pwd, u, usr, lid, uid) {
	_.post('forcePassword/' + usr, '{"usr": "' + u + '", "pass": "' +
	       CryptoJS.SHA3(pwd, {outputLength: 256}).toString(CryptoJS.enc.Hex) + '"}',
	       'application/json;charset=UTF-8',
	       function (data, ok) {
		   if (ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else f.showuser(usr, lid, uid);
		   }
	       });
    };


    f.doforceemail = function(email, nope, u, usr, lid, uid) {
	_.subst(nope, 'you are changing by force the email of ' + u + ' to ' + _.id(email).value +
		'&nbsp;&nbsp;<button onclick="socia.forceemail' + qargs(email, u, usr, lid, uid) + '">I know, go on</button>');
    };


    f.forceemail = function(email, u, usr, lid, uid) {
	_.post('forceEmail/' + usr, '{"usr": "' + u + '", "email": "' + _.id(email).value + '"}',
	       'application/json;charset=UTF-8',
	       function (data, ok) {
		   if (ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else f.showuser(usr, lid, uid);
		   }
	       });
    };


    f.role = function(role, u, usr, lid, uid) {
	_.post('role/' + usr, '{"role": "' + role + '", "usr": "' + u + '", "enabled": ' + ((_.id(role).checked)?1:0) + '}',
	       'application/json;charset=UTF-8',
	       function (data, ok) {
		   if (ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else f.showuser(usr, lid, uid);
		   }
	       });
    };


    f.doforceunpublish = function(oid, u, usr, lid, uid) {
	_.subst('b' + oid,
		'<input id="r' + oid + '" type="text" onkeyup="if(_.kid.ret)if(_.id' + qargs('r' + oid ) +
		'.value.trim().length) socia.forceunpublish' + qargs(oid, u, usr, lid, uid) +
		'" placeholder="give ' + u + ' a reason">');
    };


    f.forceunpublish = function(oid, u, usr, lid, uid) {
	_.post('forceUnpublish/' + usr, '{"usr": "' + u + '", "url": "' + _.id(oid).value + '", "descr": "' +
	       _.id('d' + oid).innerHTML + '", "reason": "' + _.id('r' + oid).value + '"}',
	       'application/json;charset=UTF-8', function (data, ok) {
		   if (ok) {
		       var resp = JSON.parse(data);

		       if(resp.err) window.location = resp.message;
		       else f.showuser(usr, lid, uid);
		   }
	       });
    };


    var  contribs = {};


    var xar = function (a, b) {
	return a.filter(function (e) {return !(b.indexOf(e) + 1);});
    };


    var qargs = function () {
	var i, s = '';

	if(arguments.length) {
	    s += "'" + arguments[0] + "'";
	    for(i = 1; i<arguments.length; i++)
		s += ", '" + arguments[i] + "'";
	}

	return '(' + s + ')';
    };


    var qargsandlist = function () {
	var i, s = '';

	s += "'" + arguments[0] + "'";
	for(i = 1; i<arguments.length - 1; i++)
	    s += ", '" + arguments[i] + "'";

	return	'(' + s + ", ['" + arguments[i].reduce(function (p, e) {return p + "', '" + e.replace(/'/g, "\\'");}) + "'])";
    };


    var invalidcontr = function (url) {
	if (url.match(/\.tif{1,2}$/i))
	    return 'TIFF image format is not accepted, sorry';

	return null;
    };


    var pubutton = function (pub, args) {
	return (pub)?
	    '&nbsp;<button onclick="socia.unpublish' + args + '">unpublish' + '</button>'
	    :
	    '&nbsp;<button onclick="socia.publish' + args + '">publish' + '</button>'
	;
    };


    var globutton = function (args) {
	return '&nbsp;<button  class="bold" onclick="socia.addgloth' + args + '">+</button>';
    };


    var themeselect = function (themes, sorted, extra) {
	if(!themes.length)
	    return '';

	var i, s = '&nbsp;' + ((extra)?'<select ' +  extra + '>':'<select>'), t = (sorted)?themes.sort():themes;

	for(i = 0; i<t.length; i++)
	    s += '<option value="' + t[i] + '">' + t[i] + '</option>';

	return s + '</select>';
    };


    var glothemes = function (url, usr, descr, pid, themes) {
	var s = (themes.length)?
	    '&nbsp;&nbsp;select an existing theme: ' +
	    themeselect(themes, true, 'id="sglo' + pid + '"') +
	    '&nbsp;<button class="bold" onclick="socia.themeadd' + qargs(usr, url, descr, pid, 'sglo' + pid) + '">+</button>' +
	    '&nbsp;<input type="text"  placeholder="or add a new one" id="aglo' + pid
	    :
	    '&nbsp;<input type="text"  placeholder="add a theme" id="aglo' + pid
	;

	return s + '" onkeyup="if(_.kid.ret) socia.themeadd' + qargs(usr, url, descr, pid, 'aglo' + pid) + '">';
    };


    var ownthemes = function (url, usr, descr, pid, themes) {
	return 	'relates to: ' +
	    themeselect(themes, false, 'id="sown' + pid + '"') +
	    '&nbsp;<button class="bold" onclick="socia.themedel' + qargs(usr, url, descr, pid) + '">&minus;</button>' +
	    '<span id="bown' + pid + '">' + globutton(qargsandlist(usr, url, descr, pid, themes)) + '&nbsp;</span>';
    };


    return f;
}();
